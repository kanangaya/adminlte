<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::resource('/pertanyaan', 'PertanyaanController');
Route::resource('/pertanyaan/create', 'PertanyaanController');
Route::resource('pertanyaan','PertanyaanController');
Route::resource('/pertanyaan/{pertanyaan_id}','PertanyaanController');
Route::resource('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController');
Route::resource('/pertanyaan/{pertanyaan_id}','PertanyaanController');
Route::resource('/pertanyaan/{pertanyaan_id}','PertanyaanController');
