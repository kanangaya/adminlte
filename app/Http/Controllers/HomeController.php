<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
    	return view('home');
    }
    public function data()
    {
    	return view('data');
    }
    public function items()
    {
    	return view('items.index');
    }
}


